import { Component } from '@angular/core';
import { ApiService } from '../../services/api/api.service';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-sales',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './sales.component.html',
  styleUrl: './sales.component.scss'
})
export class SalesComponent {

  constructor(public apiService: ApiService) { }

  salesTableData = this.apiService.sales();
  previousColSort: string = '';

  sortTable(col:string) {
    if (col === this.previousColSort) {
      this.salesTableData = this.salesTableData.reverse();
    }
    else if (col === 'customer') {
      this.salesTableData = this.salesTableData.sort((a, b) => a.customer.localeCompare(b.customer));
    }
    else if (col === 'total') {
      this.salesTableData = this.salesTableData.sort((a, b) => a.total - b.total);
    }
    else if (col === 'method') {
      this.salesTableData = this.salesTableData.sort((a, b) => a.method.localeCompare(b.method));
    }
    else if (col === 'status') {
      this.salesTableData = this.salesTableData.sort((a, b) => a.status.localeCompare(b.status));
    }
    else if (col === 'time') {
      this.salesTableData = this.salesTableData.sort((a, b) => a.time.getTime() - b.time.getTime());
    }

    this.previousColSort = col;
  }

}
