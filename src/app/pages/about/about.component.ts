import { Component } from '@angular/core';
import { ApiService } from '../../services/api/api.service';
import { timeout } from 'rxjs';

@Component({
  selector: 'app-about',
  standalone: true,
  imports: [],
  templateUrl: './about.component.html',
  styleUrl: './about.component.scss'
})

export class AboutComponent {
  pageCopy: any;

  constructor(public apiService: ApiService) { }

  ngOnInit() {
    this.apiService.getCompanyInfo();
    this.pageCopy = this.apiService.company();

  }

}
