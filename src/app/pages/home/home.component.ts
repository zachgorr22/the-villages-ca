import { Component } from '@angular/core';
import { ApiService } from '../../services/api/api.service';
import { CartService } from '../../services/cart/cart.service';

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [],
  templateUrl: './home.component.html',
  styleUrl: './home.component.scss'
})
export class HomeComponent {

  constructor(public apiService: ApiService,
              public cartService: CartService) { }


  addItem(item: any) {
    this.cartService.addItemToCart(item);
  }
}
