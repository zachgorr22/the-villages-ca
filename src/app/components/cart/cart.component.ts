import { Component, EventEmitter, Output } from '@angular/core';
import { ApiService } from '../../services/api/api.service';
import { CartService } from '../../services/cart/cart.service';
import { CommonModule} from '@angular/common';

@Component({
  selector: 'app-cart',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './cart.component.html',
  styleUrl: './cart.component.scss'
})
export class CartComponent {

  constructor(public apiService: ApiService,
              public cartService: CartService
  ) {}

  @Output() closeModal = new EventEmitter();
  cart: any
  totalCartPrice = 0;

  ngOnInit(){
    this.cart = this.cartService.getCart();
    this.calculateCartPrice();
  }

  calculateCartPrice() {
    this.totalCartPrice = 0;
    this.cart.cart.forEach((item: any) => {
      if (!item.price) {
        item.price = 0;
      }
      this.totalCartPrice += item.price;
    });
  }

  updateModal() {
    this.closeModal.emit(true);
  }

  removeItemFromCart(item: any) {
    this.cartService.removeItemFromCart(item);
    this.calculateCartPrice()
  }
}
