import { Injectable, signal } from '@angular/core';
import { Product } from '../../types/product';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  
  constructor() {}

  usersCart: any = [];

  getCart() {
    return {cart: this.usersCart};
  }

  addItemToCart(item: any) {
    this.usersCart.push(item);
  }

  removeItemFromCart(item: any){
    let itemIndex = this.usersCart.indexOf(item);
    if (itemIndex > -1) {
      this.usersCart.splice(itemIndex, 1);
    }
  }

}
